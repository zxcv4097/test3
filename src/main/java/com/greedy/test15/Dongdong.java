package com.greedy.test15;

public class Dongdong {

	private int no;
	private String name;
	
	public Dongdong() {}

	public Dongdong(int no, String name) {
		super();
		this.no = no;
		this.name = name;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Dongdong [no=" + no + ", name=" + name + "]";
	}
	
	
}
